#!/bin/env bash

curl http://localhost:8080/addtest \
    --include \
    --header "Content-Type: application/json" \
    --request "POST" \
    --data '{
    "id": 2,
    "teacherId": 2,
    "title": "new test title",
    "subjectName": "second subject",
    "author": "me",
    "questions": [
        {
            "body": "How old i am ?",
            "score": 1,
            "answers": [
                "69",
                "21",
                "22",
                "23"
            ],
            "correctAnswer": "69"
        },
        {
            "Body": "How old are you ?",
            "Score": 2,
            "Answers": [
                "69",
                "23",
                "32",
                "very old"
            ],
            "correctAnswer": "very old"
        }
    ]
}'

