.mode columns
.headers on
SELECT t.id, t.title, t.teacher_id, t.subject, t.author, t.due_date,
    q.id, q.body, q.score, q.correct_answer, q.open_ended,
    a.id, a.body
FROM tests t
LEFT JOIN questions q ON t.id = q.test_id
LEFT JOIN answers a ON q.id = a.question_id
-- WHERE t.id = 2
