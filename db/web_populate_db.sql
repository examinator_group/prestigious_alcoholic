-- Заполнение таблицы "tests"
INSERT INTO teachers (id, name)
VALUES (123, "Kolya");

INSERT INTO teachers (id, name)
VALUES (456, "Sonya");

-- Заполнение таблицы "tests"
INSERT INTO tests (teacher_id, due_date, title, subject, author)
VALUES (123, '2023-10-31 18:00:00', 'Test 1', 'Subject 1', 'Author 1');

INSERT INTO tests (teacher_id, due_date, title, subject, author)
VALUES (456, '2023-11-01 11:52:00', 'Test 2', 'Subject 2', 'Author 2');

-- Заполнение таблицы "questions"
INSERT INTO questions (test_id, body, score, correct_answer)
VALUES (1, 'Question 1', 5, 'Answer 1');

INSERT INTO questions (test_id, body, score, correct_answer)
VALUES (1, 'Question 2', 10, 'Answer 2');

INSERT INTO questions (test_id, body, score, correct_answer)
VALUES (2, 'Question 3', 8, 'Answer 3');

-- Заполнение таблицы "answers"
INSERT INTO answers (question_id, body)
VALUES (1, 'Answer 1');

INSERT INTO answers (question_id, body)
VALUES (1, 'Answer 2');

INSERT INTO answers (question_id, body)
VALUES (2, 'Answer 3');

INSERT INTO answers (question_id, body)
VALUES (2, 'Answer 4');

INSERT INTO answers (question_id, body)
VALUES (3, 'Answer 5');
