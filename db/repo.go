package repo

import (
    "database/sql"
    "errors"
    "fmt"
    "log"
    "gitlab.com/examinator_group/prestigious_alcoholic/data"
)

var LoginExistsError = errors.New("this login already exists")
var LoginDoesntExistsError = errors.New("this login doesn't exists")

type Repository struct {
    db *sql.DB
}

func CreateRepository(db *sql.DB) *Repository {
    return &Repository{
        db: db,
    }
}

func (r *Repository) CreateTable() error {
    query := `
    CREATE TABLE IF NOT EXISTS students (
    id INTEGER PRIMARY KEY,
    name TEXT ,
    surname TEXT ,
    middlename TEXT,
    organization TEXT,
    student_group TEXT,
    email TEXT,
    password TEXT
    );

    CREATE TABLE IF NOT EXISTS teachers (
    id INTEGER PRIMARY KEY,
    name TEXT ,
    surname TEXT ,
    middlename TEXT,
    position TEXT,
    organization TEXT,
    email TEXT,
    password TEXT
    );

    CREATE TABLE IF NOT EXISTS tests (
    id INTEGER PRIMARY KEY,
    teacher_id INTEGER,
    due_date TEXT,
    title TEXT,
    subject TEXT,
    author TEXT,
    visible INTEGER,
    FOREIGN KEY(teacher_id) REFERENCES teachers(id)
    );

    CREATE TABLE IF NOT EXISTS questions (
    id INTEGER PRIMARY KEY,
    test_id INTEGER,
    body TEXT,
    score INTEGER,
    correct_answer TEXT,
    open_ended INTEGER,
    FOREIGN KEY(test_id) REFERENCES tests(id)
    );

    CREATE TABLE IF NOT EXISTS answers (
    id INTEGER PRIMARY KEY,
    question_id INTEGER,
    body TEXT,
    FOREIGN KEY(question_id) REFERENCES questions(id)
    );

    CREATE TABLE IF NOT EXISTS results (
    id INTEGER PRIMARY KEY,
    test_id INTEGER,
    student_id INTEGER,
    answers TEXT,
    FOREIGN KEY(test_id) REFERENCES tests(id),
    FOREIGN KEY(student_id) REFERENCES students(id)
    );
    `

    _, err := r.db.Exec(query)
    if err != nil {
        printRedError(errors.New("error while creating tables"))
        printRedError(err)
        return err
    }
    return nil
}

func (r *Repository) InsertTest(test data.Test) (*data.Test, error) {

    res, err := r.db.Exec("INSERT INTO tests(teacher_id, due_date, title, subject, author, visible) values(?, ?, ?, ?, ?)", test.TeacherId, test.DueDate, test.Title, test.Subject, test.Author, test.Visible)
    if err != nil {
        printRedError(errors.New("error while inserting test"))
        printRedError(err)
        return nil, err
    }

    tempId, err := res.LastInsertId()
    if err != nil {
        printRedError(errors.New("error while getting last test id"))
        printRedError(err)
        return nil, err
    }

    test.Id = int(tempId)

    for i, vQuestion := range test.Questions {

        newQuestionId, err := r.insertQuestion(test.Id, vQuestion)
        if err != nil {
            printRedError(err)
            return nil, err
        }
        test.Questions[i].Id = newQuestionId

        for _, vAnswer := range vQuestion.Answers {
            err := r.insertAnswer(vQuestion.Id, vAnswer)
            if err != nil {
                printRedError(err)
                return nil, err
            }
        }

    } 

    return &test, nil
}


func (r *Repository) InsertResult(result data.Result) (*data.Result, error) {

    res, err := r.db.Exec("INSERT INTO results(test_id, student_id, answers) values(?, ?, ?)", result.TestId, result.StudentId, result.Answers)
    if err != nil {
        printRedError(errors.New("error while inserting result"))
        printRedError(err)
        return nil, err
    }

    tempId, err := res.LastInsertId()
    if err != nil {
        printRedError(errors.New("error while getting last result id"))
        printRedError(err)
        return nil, err
    }

    result.Id = int(tempId)
    return &result, nil
}

func (r *Repository) InsertTeacher(newTeacher data.Teacher) (*data.Teacher, error) {
    row := r.db.QueryRow("SELECT email FROM teachers WHERE email=?", newTeacher.Email)
    var email string = "nothing"
    if err := row.Scan(&email); err != nil {
        printRedError(errors.New("while scanning test"))
        fmt.Println("Email:")
        printRedError(errors.New(email))
        printRedError(err)
        return nil, err
    }
    fmt.Println("Email:" + email)
    if email != "nothing" {
        return nil, LoginExistsError
    }

    res, err := r.db.Exec("INSERT INTO teachers(name, surname, middlename, position, organization, email, password) values(?,?,?,?,?,?,?)", newTeacher.Name, newTeacher.Surname, newTeacher.Middlename, newTeacher.Position, newTeacher.Organization, newTeacher.Email, newTeacher.Password)
    if err != nil {
        printRedError(errors.New("error while inserting teacher"))
        printRedError(err)
        return nil, err
    }

    tempId, err := res.LastInsertId()
    if err != nil {
        printRedError(errors.New("error while getting last teacher id"))
        printRedError(err)
        return nil, err
    }

    newTeacher.Id = int(tempId)
    return &newTeacher, nil

}

func (r *Repository) InsertStudent(newStudent data.Student) (*data.Student, error) {
    row := r.db.QueryRow("SELECT email FROM students WHERE email=?", newStudent.Email)
    var email string = "nothing"
    if err := row.Scan(&email); err != nil {
        printRedError(errors.New("while scanning test"))
        fmt.Println("Email:")
        printRedError(errors.New(email))
        printRedError(err)
        return nil, err
    }
    fmt.Println("Email:" + email)
    if email != "nothing" {
        return nil, LoginExistsError
    }

    res, err := r.db.Exec("INSERT INTO students(name, surname, middlename, organization, student_group, email, password) values(?,?,?,?,?,?,?)", newStudent.Name, newStudent.Surname, newStudent.Middlename, newStudent.StudentGroup, newStudent.Email, newStudent.Password)
    if err != nil {
        printRedError(errors.New("error while inserting teacher"))
        printRedError(err)
        return nil, err
    }

    tempId, err := res.LastInsertId()
    if err != nil {
        printRedError(errors.New("error while getting last teacher id"))
        printRedError(err)
        return nil, err
    }

    newStudent.Id = int(tempId)
    return &newStudent, nil

}

func (r *Repository) insertQuestion(testId int, question data.Question) (int, error) {
    res, err := r.db.Exec("INSERT INTO questions(test_id, body, score, correct_answer, open_ended) values(?, ?, ?, ?, ?)", testId, question.Body, question.Score, question.CorrectAnswer, question.OpenEnded)
    if err != nil {
        printRedError(errors.New("error while inserting question"))
        printRedError(err)
        return -1, err
    }

    tempId, err := res.LastInsertId()
    if err != nil {
        printRedError(errors.New("error while getting last question id"))
        printRedError(err)
        return -1, err
    }

    return int(tempId), nil
}

func (r *Repository) insertAnswer (questionId int, answer string ) error {
    _, err := r.db.Exec("INSERT INTO answers(question_id, body) values(?,?)", questionId, answer) 
    if err != nil {
        printRedError(errors.New("error while inserting answer"))
        printRedError(err)
        return err
    }
    return nil
}

func (r *Repository) GetTestsByTeacherId(teacherId int) (*data.Test, error) {
    row := r.db.QueryRow("SELECT * FROM tests WHERE teacher_id=?", teacherId)

    var test data.Test
    if err := row.Scan(&test.Id, &test.TeacherId, &test.DueDate, &test.Title, &test.Subject, &test.Author, &test.Visible); err != nil {
        printRedError(errors.New("while scanning test"))
        printRedError(err)
        return nil, err
    }

    questions, err := r.getQuestionsForTest(test.Id)
    if err!=nil {
        printRedError(err)
        return nil, err
    }

    test.Questions = append(test.Questions, questions...)

    return &test, nil
}

func (r *Repository) GetTestById(id int) (*data.Test, error) {
    row := r.db.QueryRow("SELECT * FROM tests WHERE id=?", id)

    var test data.Test
    if err := row.Scan(&test.Id, &test.TeacherId, &test.DueDate, &test.Title, &test.Subject, &test.Author); err != nil {
        printRedError(errors.New("while scanning test"))
        printRedError(err)
        return nil, err
    }

    questions, err := r.getQuestionsForTest(test.Id)
    if err!=nil {
        printRedError(err)
        return nil, err
    }
    test.Questions = append(test.Questions, questions...)

    return &test, nil
}

func (r *Repository) GetStudentById(id int) (*data.Student, error) {
    row := r.db.QueryRow("SELECT * FROM students WHERE id=?", id)

    var student data.Student
    if err := row.Scan(&student.Id, &student.Name, &student.Surname, &student.Middlename, &student.StudentGroup, &student.Email, &student.Password); err != nil {
        printRedError(errors.New("while scanning test"))
        printRedError(err)
        return nil, err
    }
    student.Email = ""
    student.Password = ""
    return &student, nil
}

func (r *Repository) GetAllTestsForTeacher(userId int) ([]data.Test, error) {
    tests := []data.Test{}

    rows, err := r.db.Query("SELECT * FROM tests WHERE teacher_id=?", userId)
    defer rows.Close()
    if err != nil {
        printRedError(errors.New("All tests bad"))
        printRedError(err)
        return nil, err
    }
    
    for rows.Next() {
        var test data.Test
        if err := rows.Scan(&test.Id, &test.TeacherId, &test.DueDate, &test.Title, &test.Subject, &test.Author); err != nil {
            printRedError(errors.New("Parsing all tests bad"))
            printRedError(err)
            return nil, err
        }

        questions, err := r.getQuestionsForTest(test.Id)
        if err!=nil {
            printRedError(err)
            return nil, err
        }
        test.Questions = append(test.Questions, questions...)

        tests = append(tests, test)
    }
    return tests, nil
}

func (r *Repository) GetAllVisibleTests() ([]data.Test, error) {
    tests := []data.Test{}

    rows, err := r.db.Query("SELECT * FROM tests WHERE visible=1")
    defer rows.Close()
    if err != nil {
        printRedError(errors.New("All tests bad"))
        printRedError(err)
        return nil, err
    }
    
    for rows.Next() {
        var test data.Test
        if err := rows.Scan(&test.Id, &test.TeacherId, &test.DueDate, &test.Title, &test.Subject, &test.Author); err != nil {
            printRedError(errors.New("Parsing all tests bad"))
            printRedError(err)
            return nil, err
        }

        questions, err := r.getQuestionsForTest(test.Id)
        if err!=nil {
            printRedError(err)
            return nil, err
        }
        test.Questions = append(test.Questions, questions...)

        tests = append(tests, test)
    }
    return tests, nil
}

func (r *Repository) GetResultsForTest(testId int) ([]data.Result, error) {
    results := []data.Result{}

    rows, err := r.db.Query("SELECT * FROM results WHERE test_id=?", testId)
    defer rows.Close()
    if err != nil {
        printRedError(errors.New("Error while gettings results for test"))
        printRedError(err)
        return nil, err
    }
    
    for rows.Next() {
        var relust data.Result
        if err := rows.Scan(&relust.Id, &relust.TestId, &relust.StudentId, &relust.Answers); err != nil {
            printRedError(errors.New("Error while parsing results for test "))
            printRedError(err)
            return nil, err
        }


        results = append(results, relust)
    }
    return results, nil
}

func (r *Repository) getQuestionsForTest(testId int) ([]data.Question, error) {
    questions := []data.Question{}

    rows, err := r.db.Query("SELECT id, body, score, correct_answer, open_ended FROM questions WHERE test_id=?", testId)
    defer rows.Close()
    if err != nil {
        printRedError(errors.New("while querying questions"))
        printRedError(err)
        return nil, err
    }

    for rows.Next() {
        var question data.Question
        if err := rows.Scan(&question.Id, &question.Body, &question.Score, &question.CorrectAnswer, &question.OpenEnded); err!=nil {
            printRedError(errors.New("while scanning questions"))
            printRedError(err)
            return nil, err
        }

        if !question.OpenEnded {
            answers, err := r.getAnswersForQuestion(question.Id)
            if err != nil {
                return nil, err
            }
            question.Answers = answers
        }

        questions = append(questions, question)
    }

    return questions, nil

}

func (r *Repository) getAnswersForQuestion(questionId int) ([]string, error) {
    answers := []string{}

    rows, err := r.db.Query("SELECT body FROM answers WHERE question_id=?", questionId)
    if err!=nil {
        printRedError(errors.New("while querying answers"))
        printRedError(err)
        return nil, err
    }

    for rows.Next() {
        var answer string
        if err := rows.Scan(&answer); err != nil {
            printRedError(errors.New("while scanning answers"))
            printRedError(err)
            return nil, err
        }

        answers = append(answers, answer)
    }

    return answers, nil
}

func (r *Repository) CheckLogin(email string, userType string) (int, string, error) {
    if userType == data.TeacherType {
        row := r.db.QueryRow("SELECT id, password FROM teachers WHERE email=?", email)

        var id int
        var password string

        if err := row.Scan(&id, &password); err != nil {
            return -1, "", err
        }
        return id, password, nil
    } else if userType == data.StudentType {

        row := r.db.QueryRow("SELECT id, password FROM students WHERE email=?", email)

        var id int
        var password string

        if err := row.Scan(&id, &password); err != nil {
            return -1, "", err
        }

        return id, password, nil

    } else {
        return -1, "", LoginDoesntExistsError
    }
}


func printRedError(err error) {
    red := "\033[31m"
    escape := "\033[0m"
    stringError := fmt.Sprintf("%s", err)

    log.Println(red + stringError + escape)
}
