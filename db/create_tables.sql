DROP TABLE IF EXISTS students;
CREATE TABLE IF NOT EXISTS students (
    id INTEGER PRIMARY KEY,
    name TEXT ,
    surname TEXT ,
    middlename TEXT,
    organization TEXT,
    student_group TEXT, 
    email TEXT,
    password TEXT
);

DROP TABLE IF EXISTS teachers;
CREATE TABLE IF NOT EXISTS teachers (
    id INTEGER PRIMARY KEY,
    name TEXT ,
    surname TEXT ,
    middlename TEXT,
    position TEXT,
    organization TEXT,
    email TEXT,
    password TEXT
);

DROP TABLE IF EXISTS tests;
CREATE TABLE IF NOT EXISTS tests (
    id INTEGER PRIMARY KEY,
    teacher_id INTEGER,
    due_date TEXT,
    title TEXT,
    subject TEXT,
    author TEXT,
    visible INTEGER,
    FOREIGN KEY(teacher_id) REFERENCES teachers(id)
);

DROP TABLE IF EXISTS questions;
CREATE TABLE IF NOT EXISTS questions (
    id INTEGER PRIMARY KEY,
    test_id INTEGER,
    body TEXT,
    score INTEGER,
    correct_answer TEXT,
    open_ended INTEGER,
    FOREIGN KEY(test_id) REFERENCES tests(id)
);

DROP TABLE IF EXISTS answers;
CREATE TABLE IF NOT EXISTS answers (
    id INTEGER PRIMARY KEY,
    question_id INTEGER,
    body TEXT,
    FOREIGN KEY(question_id) REFERENCES questions(id)
);

DROP TABLE IF EXISTS results;
CREATE TABLE IF NOT EXISTS results (
    id INTEGER PRIMARY KEY,
    test_id INTEGER,
    student_id INTEGER,
    answers TEXT,
    FOREIGN KEY(test_id) REFERENCES tests(id),
    FOREIGN KEY(student_id) REFERENCES students(id)
);
