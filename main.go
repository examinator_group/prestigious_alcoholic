package main

import (
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"fmt"
	"strconv"

	// "encoding/json"
	// "fmt"
	"log"
	"net/http"
	"os"
	"time"

	// "strconv"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/examinator_group/prestigious_alcoholic/data"
	repo "gitlab.com/examinator_group/prestigious_alcoholic/db"
)

var secretKey = []byte("ChangeMe")
var repository *repo.Repository

func main() {

    secret := os.Getenv("JWT_SECRET")
    secretKey = []byte(secret)

    portVar := os.Getenv("TEST_SERVER_PORT")
    port, err := strconv.Atoi(portVar)
    if err != nil {
        port = 8080
    }


    db, err := sql.Open("sqlite3", "db/test.db")
    if err != nil {
        panic("smth is wrong with DB")
    }

    defer db.Close()
    repository = repo.CreateRepository(db)
    err = repository.CreateTable()
    if err != nil {
        log.Println(err)
        os.Exit(1)
    }

    router := gin.Default()

    router.POST("/login", login)
    router.POST("/registerTeacher", registerTeacher)
    router.POST("/registerStudent", registerStudent)

    router.GET("/getTeachersTests", verifyJWT(getAllTestsForTeacher))
    router.GET("/getTests", verifyJWT(getAllTestsForTeacher))
    router.GET("/getTest/:id", verifyJWT(getTestById))
    router.GET("/getResults/:id", verifyJWT(getResults))
    router.GET("/getStudent/:id", verifyJWT(getStudent))

    router.POST("/addTest", verifyJWT(addTest))
    router.POST("/addResult", verifyJWT(addResult))
    
    path := fmt.Sprintf("localhost:%d", port)
    router.Run(path)

}

func registerTeacher(c *gin.Context) {
    var newTeacher data.Teacher
    if err := c.BindJSON(&newTeacher); err != nil {
        c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "incorrect data"})
        return 
    }
    
    newTeacher.Password = hashPassword(newTeacher.Password)

    teacher, err := repository.InsertTeacher(newTeacher)
    if err != nil {
        if err == repo.LoginExistsError {
            c.IndentedJSON(http.StatusConflict, gin.H{"message": "login already taken" })
        } else {
            c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "smth wrong on server"})
        }
        
    }

    token, err := generateJWT(teacher.Id, data.TeacherType)
    if err != nil {
        c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "error while creating token"})
    }

    c.IndentedJSON(http.StatusOK, token)
}


func registerStudent(c *gin.Context) {
    var newStudent data.Student
    if err := c.BindJSON(&newStudent); err != nil {
        c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "incorrect data"})
        return 
    }
    
    newStudent.Password = hashPassword(newStudent.Password)

    student, err := repository.InsertStudent(newStudent)
    if err != nil {
        if err == repo.LoginExistsError {
            c.IndentedJSON(http.StatusConflict, gin.H{"message": "login already taken" })
        } else {
            c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "smth wrong on server"})
        }
    }

    token, err := generateJWT(student.Id, data.StudentType)
    if err != nil {
        c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "error while creating token"})
    }

    c.IndentedJSON(http.StatusOK, token)
}

func login(c *gin.Context) {
    var loginData data.LoginData
    if err := c.BindJSON(&loginData); err != nil {
        c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "login data expected"})
        return 
    }

    userId, password, err := repository.CheckLogin(loginData.Login, loginData.UserType)
    if err != nil {
        c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "no such account found"})
        return 
    }
    
    pass := hashPassword(loginData.Password)
    if password != pass {
        c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "wrong password"})
        return 
    }

    token, err := generateJWT(userId, loginData.UserType)
    c.IndentedJSON(http.StatusOK, token)
}

func addTest(c *gin.Context, userId int, userType string) {

    if userType != data.TeacherType {
        c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "you're not authorized for this request"})
        return 
    }

    var newTest data.Test

    if err := c.BindJSON(&newTest); err != nil {
        c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "test expected"})
        return
    }

    test, err := repository.InsertTest(newTest)
    if err != nil {
        c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "i've done smth bad"})
        return 
    }
    c.IndentedJSON(http.StatusCreated, test)
}

func addResult(c *gin.Context, userId int, userType string) {

    if userType != data.StudentType {
        c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "you're not authorized for this request"})
        return 
    }

    var newResult data.Result

    if err := c.BindJSON(&newResult); err != nil {
        c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "result expected"})
        return
    }

    result, err := repository.InsertResult(newResult)
    if err != nil {
        c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "i've done smth bad"})
        return 
    }
    c.IndentedJSON(http.StatusCreated, result)
}

func getResults(c *gin.Context, userId int, userType string) {

    if userType != data.TeacherType  {
        c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "you're not authorized for this request"})
        return 
    }

    id, err := strconv.Atoi(c.Param("id"))
    if err != nil {
        c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "id needs to be a number"})
        return 
    }

    test, err := repository.GetTestById(id)
    if err != nil {
        c.IndentedJSON(http.StatusNotFound, gin.H{"message": "test doesn't exists"})
        return 
    }

    if test.TeacherId != userId {
        c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "you're not authorized for this request"})
        return 
    }
    
    results, err := repository.GetResultsForTest(id)
    if err != nil {
        c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "smth went wrong"})
        return 
    }

    c.IndentedJSON(http.StatusOK, results)
}

func getAllTestsForTeacher(c *gin.Context, userId int, userType string) {

    if userType != data.TeacherType {
        c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "you're not authorized for this request"})
        return 
    }

    tests, err := repository.GetAllTestsForTeacher(userId)
    if err != nil {
        c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "smth went wrong"})
        return 
    }

    c.IndentedJSON(http.StatusOK, tests)
}

func getAllVisibleTests(c *gin.Context, userId int, userType string) {
    tests, err := repository.GetAllVisibleTests()
    if err != nil {
        c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "smth went wrong"})
        return 
    }

    c.IndentedJSON(http.StatusOK, tests)
}

func getTestById(c *gin.Context, userId int, userType string) {
    id, err := strconv.Atoi(c.Param("id"))
    if err != nil {
        c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "id needs to be a number"})
        return 
    }

    test, err := repository.GetTestById(id)

    if err != nil {
        c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "smth went wrong"})
        return 
    }
        
    if test.Visible != 1 {
        if userType == data.StudentType {
            c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "no access for you"})
            return 
        } else if userId != test.TeacherId {
            c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "no access for you"})
            return 
        }
    }
    
    if userType == data.StudentType {
        for i,_ := range test.Questions {
            test.Questions[i].CorrectAnswer = ""
        }
    }

    c.IndentedJSON(http.StatusOK, test)
}

func getStudent(c *gin.Context, userId int, userType string) {
    id, err := strconv.Atoi(c.Param("id"))
    if err != nil {
        c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "id needs to be a number"})
        return 
    }

    student, err := repository.GetStudentById(id)

    if err != nil {
        c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "smth went wrong"})
        return 
    }
        
    c.IndentedJSON(http.StatusOK, student)
}


func generateJWT(userId int, userType string) (string, error) {
    token := jwt.New(jwt.SigningMethodEdDSA)

    claims := token.Claims.(jwt.MapClaims)
    claims["exp"] = time.Now().Add(8 * time.Hour) 
    claims["user_id"] = userId
    claims["user_type"] = userType

    return token.SignedString(secretKey)
}

func verifyJWT(endpointHandler func(c *gin.Context, userId int, userType string)) gin.HandlerFunc {

    return gin.HandlerFunc(func(c *gin.Context) {

        header := c.Request.Header.Get("Token")
        if header == "" {
            c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "You're Unauthorized due to No token in the header"})
            return 
        }

        token, err := jwt.Parse(header, func(innerToken *jwt.Token) (interface{}, error) {
            _, ok := innerToken.Method.(*jwt.SigningMethodECDSA)
            if !ok {
                c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "You're unauthorized due to error with signing method"})
            }
            return "", nil
        })
        if err != nil {
            c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "You're Unauthorized due to error parsing the JWT"})
            return 
        }


        claims, ok := token.Claims.(jwt.MapClaims)
        if ok && token.Valid {
            userId := claims["user_id"].(int)
            userType := claims["user_type"].(string)

            endpointHandler(c, userId, userType)
        } else {
            c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "You're Unauthorized due to invalid Token"})
            return 
        }

    })

}

func hashPassword(password string) string {
	hash := sha256.New()
	hash.Write([]byte(password))
	return hex.EncodeToString(hash.Sum(nil))
}

