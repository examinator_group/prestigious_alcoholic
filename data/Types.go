package data

type Test struct {
    Id int `json:"id"`
    TeacherId int `json:"teacher_id"`
    Title string `json:"title"`
    Subject string `json:"subject"`
    Author string `json:"author"`
    Questions []Question `json:"questions"`
    DueDate string `json:"due_date"`
    Visible int `json:"visible"`
}

type Question struct {
    Id int `json:"id"`
    Body string `json:"body"`
    Score int `json:"score"`
    Answers []string `json:"answers"`
    CorrectAnswer string `json:"correctAnswer"`
    OpenEnded bool `json:"open_ended"`
}

type Result struct {
    Id int `json:"id"`
    TestId int `json:"test_id"`
    StudentId int `json:"student_id"`
    Answers []string `json:"answers"`
}

type Teacher struct {
    Id int `json:"id"`
    Name string `json:"name"`
    Surname string `json:"surname"` 
    Middlename string `json:"middlename"`
    Position string `json:"position "`
    Organization string `json:"organization"`
    Email string `json:"email "`
    Password string `json:"password "`
}

type Student struct {
    Id int `json:"id"`
    Name string  `json:"name"`
    Surname string  `json:"surname"`
    Middlename string `json:"middlename"`
    StudentGroup string `json:"student_group"`
    Email string `json:"email"`
    Password string `json:"password"`
}

type LoginData struct {
    UserType string `json:"user_type"`
    Login string `json:"login"`
    Password string `json:"password"`
}

const TeacherType = "teacher"
const StudentType = "student"
