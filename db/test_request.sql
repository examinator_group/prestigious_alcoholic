.mode columns
.headers on
SELECT t.id, t.title, q.id, q.body, a.id, a.body
FROM tests t
JOIN questions q ON t.id = q.test_id
JOIN answers a ON q.id = a.question_id
WHERE t.id = 1
